// Anderson Adon, Eugene Asare, aadon1, easare3
// ppmIO.c
// handles file rading & writing and pixel editing

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppmIO.h"
#include "commandUtil.h"
#include "imageManip.h"

/* read PPM formatted image from a file (assumes fp != NULL) */
Image* readPPM(FILE *fp) {
  Image* ppm_image;
  char buffer_P6[32];
  char current_char;
  int rgb_component;

  // gets the magic number representing the file type for the image and checks if it is P6
  if (!fgets(buffer_P6, sizeof(buffer_P6) + 1, fp)) {
    return NULL;
  } else if (buffer_P6[0] != 'P' || buffer_P6[1] != '6') {
    return NULL;
  }

  // allocates memory for image and fails if it doesn't
  ppm_image = (Image *) malloc(sizeof(Image));
  if (!ppm_image) {
    return NULL;
  }

  // the loop ignores comments
  current_char = fgetc(fp);
  while (current_char == '#') {
    while (current_char != '\n') { // if the next char is not a newline, get the next char
      current_char = fgetc(fp);
    }
  }

  ungetc(current_char, fp); // get rid of new line char that was read
  if (fscanf(fp, "%d %d", &ppm_image->cols, &ppm_image->rows) != 2) { // error if the dimensions are scanned    
    return NULL;
  } else if (fscanf(fp, "%d", &rgb_component) != 1) {
    return NULL;
  } else if (rgb_component != 255) {
    return NULL;
  }

  while (fgetc(fp) != '\n'); // skip any new line characters

  // map data properly to the image struct
  ppm_image->data = (Pixel*) malloc(ppm_image->rows * ppm_image->cols * sizeof(Pixel));
  
  if(!ppm_image) {
    return NULL;
  } else if ((int) fread(ppm_image->data, ppm_image->rows*3, ppm_image->cols, fp) < 0) {
    return NULL;
  }

  // finish file changes
  fclose(fp);
  return ppm_image;
}

/* write PPM formatted image to a file (assumes fp != NULL and img != NULL) */
int writePPM(FILE *fp, const Image* im) {

  // abort if either file pointer is dead or image pointer is null; indicate failure with -1
  if (!fp || !im) {
    return -1;
  }

  // write tag and dimensions; colors is always 255
  fprintf(fp, "P6\n%d %d\n%d\n", im->cols, im->rows, 255);

  // write pixels, return the amount of elements successfully written
  return fwrite(im->data, 3*(im->rows), (im->cols), fp);
}
