// Anderson Adon, Eugene Asare, aadon1, easare3
// imageManip.h
// header file for imageManip.c

#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H


int swapper(FILE *fp, Image* im);
int blackout(FILE *fp, Image* im);
int grayscale(FILE *fp, Image* im);
unsigned char adjust(unsigned char rgb_value, double adjustment);
int contrast(FILE *fp, Image* im, double adjustment);
int crop(FILE *fp , Image* im, int cols_start, int rows_start, int num_cols, int num_rows);

#endif
