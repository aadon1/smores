// Anderson Adon, Eugene Asare, aadon1, easare3
// commandUtil.h
// header file for commandUtil.c
#include "ppmIO.h"
#ifndef COMMANDUTIL_H
#define COMMANDUTIL_H


/* This function handles the opening and reading of the image files, storing the image data, applying operations,
   and giving back the result file. */
void free_err(FILE* output, Image* data);
int customizeFile(char* input_filename, char* output_filename, char* arguments[], int args);


#endif
