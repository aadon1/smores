// Anderson Adon, Eugene Asare, aadon1, easare3
// commandUtil.c
// Basically the interface of program
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppmIO.h"
#include "commandUtil.h"
#include "imageManip.h"

/* Read the image file, store its data, apply some effects, and then save it */
int customizeFile(char* input_filename, char* output_filename, char* arguments[], int args) {
  FILE* input_image = NULL;
  FILE* output_image = NULL;
  Image* image_data;
    
  // open input file
  input_image = fopen(input_filename, "r");
  if (!input_image) { return 2; }

  // create output file 
  output_image = fopen(output_filename, "w");
  if (!output_image) { fclose(input_image); return 4;  }

  // read the image data
  image_data = readPPM(input_image);
  if (!image_data) { fclose(input_image); fclose(output_image); return 3; }

  // if statements that determine the command to be done 
  if (strcmp(arguments[0], "swap") == 0) {
    if (args != 4) { free_err(output_image, image_data); return 6; } // if user put extra unnecessary args, return error
    
    int count = swapper(output_image, image_data);
    if (count != image_data->cols) { free_err(output_image, image_data); return 4; } // return error if function did not write the amount of data it was supposed to
  }
  
  else if (strcmp(arguments[0], "blackout") == 0) {
    if (args != 4) { free_err(output_image, image_data); return 6; } // if user put extra unnecessary args, return error
    
    int count = blackout(output_image, image_data);
    if (count != image_data->cols) { free_err(output_image, image_data); return 4; }
  }
  
  else if (strcmp(arguments[0], "crop") == 0) {
    if (args != 8) { free_err(output_image, image_data); return 6; } // if user put extra unnecessary args or not enough, return error
    
    long int x_L, y_L, x_R, y_R, num_cols, num_rows;
    if (sscanf(arguments[1], "%ld", &x_L) < 1) { free_err(output_image, image_data); return 6; }
    else if (sscanf(arguments[2], "%ld", &y_L) < 1) { free_err(output_image, image_data); return 6; }
    else if (sscanf(arguments[3], "%ld", &x_R) < 1) { free_err(output_image, image_data); return 6; }
    else if (sscanf(arguments[4], "%ld", &y_R) < 1) { free_err(output_image, image_data); return 6; }
    else {
      num_cols = (x_R - 1) - x_L;
      num_rows = (y_R - 1) - y_L;
      if (num_cols < 0 || num_rows < 0 || num_cols > image_data->cols || num_rows > image_data->rows) { free_err(output_image, image_data); return 6; }
      else if (x_L < 0 || x_R < 0 || y_L < 0 || y_R < 0) { free_err(output_image, image_data); return 6; }
    }

    int count = crop(output_image, image_data, x_L, y_L, num_cols, num_rows);
    if (count != num_cols) { free_err(output_image, image_data); return 4; }
  }
  
  else if (strcmp(arguments[0], "grayscale") == 0) {
    if (args != 4) { free_err(output_image, image_data); return 6; } // if user put extra unnecessary args, return error
    
    int count = grayscale(output_image, image_data);
    if (count != image_data->cols) { free_err(output_image, image_data); return 4; }
  }
  
  else if (strcmp(arguments[0], "contrast") == 0) {
    if (args != 5) { free_err(output_image, image_data); return 6; } // if user put extra unnecessary args or not enough return error

    double adjustment_factor = 1.0;
    sscanf(arguments[1], "%lf", &adjustment_factor); 
    
    int count = contrast(output_image, image_data, adjustment_factor);
    if (count != image_data->cols) { free_err(output_image, image_data); return 4; }
  } else { free_err(output_image, image_data); return 5;
  }

  free(image_data->data);
  free(image_data);
  fclose(output_image);
  return 0;
}

void free_err(FILE* output, Image* data) {
  fclose(output);
  free(data->data);
  free(data);
}
