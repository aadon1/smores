// Anderson Adon, Eugene Asare, aadon1, easare3
// perform file editing task

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppmIO.h"
#include "commandUtil.h"
#include "imageManip.h"

int swapper(FILE *fp, Image* im) {

  unsigned char temp = 0; // temporary holder for swapping
  // loops through each pixel's rgb value and swaps r to b, g to r, and b to g
  for (int i = 0; i < im->rows; i++) {
    for (int j = 0; j < im->cols; j++) {
      temp = im->data[i * im->cols + j].b;
      im->data[i * im->cols + j].b = im->data[i * im->cols + j].r;
      im->data[i * im->cols + j].r = im->data[i * im->cols + j].g;
      im->data[i * im->cols + j].g = temp;
    }
  }

  return writePPM(fp, im);
}

int blackout(FILE *fp, Image* im) {

  // determining from where and to where to blackout
  int middle_row = im->rows / 2;
  int middle_col = 0;
  if (im->cols % 2 == 0) {
    middle_col = im->cols / 2;
  } else {
    middle_col = im->cols / 2 + 1;
  }

  // loop through specified quadrant, change all rgb values to 0
  for (int i = 0; i < middle_row; i++) {
    for (int j = middle_col; j < im->cols; j++) {
      im->data[i * im->cols + j].b = 0;
      im->data[i * im->cols + j].r = 0;
      im->data[i * im->cols + j].g = 0;
    }
  }

  return writePPM(fp, im);
}

int grayscale(FILE *fp, Image* im) {

  // variable to hold our intensity value
  unsigned char intensity = 0;

  // loop through each pixel and set the each r, g, and b value to the calculated intensity
  for (int i = 0; i < im->rows; i++) {
    for (int j = 0; j < im->cols; j++) {
      intensity = (unsigned char) (0.3 * im->data[i * im->cols + j].r + 0.59 * im->data[i * im->cols + j].g + 0.11 * im->data[i * im->cols + j].b);
      im->data[i * im->cols + j].b = intensity;
      im->data[i * im->cols + j].r = intensity;
      im->data[i * im->cols + j].g = intensity;
    }
  }

  return writePPM(fp, im);
}

unsigned char adjust(unsigned char rgb_value, double adjustment) {

  // convert range from 0 to 255 to -0.5 to 0.5
  double new_rgb = ((( (double) rgb_value ) * (0.5 + 0.5)) / 255) - 0.5;
  new_rgb = new_rgb * adjustment;

  // saturation math
  if (new_rgb > 0.5) { new_rgb = 0.5; }
  else if (new_rgb < -0.5) { new_rgb = -0.5; }

  // convert back to normal range
  unsigned char rgb = ((( new_rgb + 0.5 ) * (255)) / (0.5 + 0.5));

  return rgb;
}

int contrast(FILE *fp, Image* im, double adjustment) {

  // loop through each pixel and set the each r, g, and b value to the adjusted contrast
  for (int i = 0; i < im->rows; i++) {
    for (int j = 0; j < im->cols; j++) {
      im->data[i * im->cols + j].b = adjust(im->data[i * im->cols + j].b, adjustment);
      im->data[i * im->cols + j].r = adjust(im->data[i * im->cols + j].r, adjustment);
      im->data[i * im->cols + j].g = adjust(im->data[i * im->cols + j].g, adjustment);
    }
  }

  return writePPM(fp, im);
}

int crop(FILE *fp , Image* im, int cols_start, int rows_start, int num_cols, int num_rows) {
  Image * crp_im;

  crp_im = (Image*) malloc(sizeof(Image));
  if (!crp_im) { return 4; }
  
  crp_im->rows = num_rows;
  crp_im->cols = num_cols;
  
  crp_im->data = (Pixel*) malloc(num_rows * num_cols * sizeof(Pixel));
  if (!crp_im->data) { return 4; }
  
  for (int i = 0; i < num_cols; i++) {
    for (int j = 0; j < num_rows; j++) {
      int ref_i = i + cols_start;
      int ref_j = j + rows_start;
      
      unsigned char r_color = im->data[ref_j * im->cols + ref_i].r;
      unsigned char g_color = im->data[ref_j * im->cols + ref_i].g;
      unsigned char b_color = im->data[ref_j * im->cols + ref_i].b;

      crp_im->data[j * crp_im->cols + i].r = r_color;
      crp_im->data[j * crp_im->cols + i].g = g_color;
      crp_im->data[j * crp_im->cols + i].b = b_color;
    }
  }

  return writePPM(fp, crp_im);
}
