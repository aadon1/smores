// Anderson Adon, Eugene Asare, aadon1, easare3
// project.c
// main function
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "commandUtil.h"
#include "ppmIO.h"
#include "imageManip.h"

int main(int argc, char* argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Failed to supply input filename.\n");
    return 1;
  }
  if (argc < 3) {
    fprintf(stderr, "Failed to supply output filename.\n");
    return 1;
  }
  if (argc < 4) {
    fprintf(stderr, "No operation name was specified.\n");
    return 5;
  }

  // get the parameters past output filename and run program
  char* parameters[argc - 3];
  for (int i = 0; i < argc - 3; i++) { parameters[i] = argv[i + 3]; }
  
  int error_code = customizeFile(argv[1], argv[2], parameters, argc);
  
  switch(error_code) {
  case 2:
    fprintf(stderr, "Specified input file could not be found.\n");
    return error_code;
    break;
  case 3:
    fprintf(stderr, "Specified input is not a proper PPM file, or reading it has failed.\n");
    return error_code;
    break;
  case 4:
    fprintf(stderr, "Specified output file could not be opened for writing, or reading it has failed.\n");
    return error_code;
    break;
  case 6:
    fprintf(stderr, "Incorrect number of arguments or kinds of arguments given for the specified operation.\n");
    return error_code;
    break;
  case 7:
    fprintf(stderr, "Arguments for crop operation were out of range for given input image.\n");
    return error_code;
    break;
  case 8:
    fprintf(stderr, "Error while running program.\n");
    return error_code;
    break;
  default:
    return 0;
  }
}
